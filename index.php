<?php
require_once('./PSWebServiceLibrary.php');
define('DEBUG', true);
define('PS_SHOP_PATH', 'http://localhost/webservice/');
define('PS_WS_AUTH_KEY', 'ELXHN2NUP31Y6M6RM1S6BANRJF3Z39KB');

$webserv = new PrestaShopWebservice(PS_SHOP_PATH, PS_WS_AUTH_KEY, DEBUG);

try{
    $data_option_attr = array("attribute"=>'Taille');
    $option_id = get_option_id($data_option_attr);
    $data = array("price"=>30,
                    "name" =>"test",
                    "description" => "Description",
                    "code" => "reference 198",
                    "category_id" => 3,
                    "quantity" => 100,
                    "id_attribute_group" => $option_id,
                    "attribute_child_value" => 'valeur_attribut',
    );
    make_product($data);
} catch (PrestaShopWebserviceException $e) {
    // Here we are dealing with errors
    $trace = $e->getTrace();
    if ($trace[0]['args'][0] == 404) echo 'Bad ID';
    else if ($trace[0]['args'][0] == 401) echo 'Bad auth key';
    else echo '<b>ERROR:</b> ' . $e->getMessage();
}

function make_product($data){
    $webService =$GLOBALS['webserv'];
    $config = PS_SHOP_PATH;

    try{
        $xml                                                                  = $webService->get(array('url' => $config.'api/products?schema=blank'));
        $product                                                              = $xml->children()->children();
        $product->price                                                       = $data["price"]; //Prix TTC
        $product->wholesale_price                                             =$data["price"]; //Prix d'achat
        $product->active                                                      = 1;
        $product->on_sale                                                     = 1; //on ne veux pas de bandeau promo
        $product->show_price                                                  = 1;
        $product->available_for_order                                         = 1;

        $product->name->language[0]                                           = $data["name"];
        $product->name->language[0][0]['id']                                  = 1;

        $product->description->language[0][0]                                 = $data["description"];
        $product->description->language[0][0]['id']                           = 1;
        $product->minimal_quantity = 1;
        $product->description_short->language[0]                           = $data["description"];
        $product->description_short->language[0][0]['id']                     = 1;
        $product->reference                                                   = $data["code"];
        $product->associations->categories->addChild('category')->addChild('id', $data["category_id"]);
        $product->id_category_default                                         = $data["category_id"];
        $product->visibility = 'both';
        $product->state = 1;
        $product->associations->stock_availables->stock_available->quantity = 100;

        //$resource_product->quantity = 10;           // la cantidad hay que setearla por medio de un webservice particular




        $opt                                                                  = array('resource' => 'products');
        $opt['postXml']                                                       = $xml->asXML();
        sleep(1);
        $xml                                                                  = $webService->add($opt);
        $product                                                              = $xml->product;
        $to_product_option = array(
            "id_attribute_group" => $data['id_attribute_group'],
            "name" => $data['attribute_child_value']
        );

        $option_id = make_product_options($to_product_option);

        $to_combinate = array(
            "option_id" =>$option_id,
            "code" => 'reference_produit_decl',
            "id_product" =>$product->id,
            "price" => 100,
            "quantity" =>1,
        );
        add_combination($to_combinate);




    } catch (PrestaShopWebserviceException $e){
        die('error');
    }
    //insert stock
    return $product->id;
}

function get_option_id($data){

    $webService =$GLOBALS['webserv'];
    $config = PS_SHOP_PATH;
    $xml_opt = $webService->get(array('url' => $config.'api/product_options'));
    $all_attr = $xml_opt->children()->children();
    foreach ($all_attr->product_option as $ttr){
        $xml_opt_val = $webService->get(array('url' => $config.'api/product_options/'.$ttr['id']));
        if($xml_opt_val->product_option->name->language[0] == $data['attribute']){
            return $xml_opt_val->product_option->name->language[0]['id'];
        }
    }

}

function add_combination($data){
    $webService =$GLOBALS['webserv'];
    $config = PS_SHOP_PATH;
    try{

        $xml                                                                            = $webService->get(array('url' => $config.'api/combinations?schema=blank'));
        $combination                                                                    = $xml->children()->children();
        $combination->associations->product_option_values->product_option_values[0]->id = $data["option_id"];
        $combination->reference                                                         = $data["code"];
        $combination->id_product                                                        = $data["id_product"];
        $combination->price                                                             = $data["price"]; //Prix TTC
        $combination->show_price                                                        = 1;
        $combination->quantity                                                          = $data["quantity"]; //Prix TTC
        $combination->minimal_quantity                                                  = 1;
        //$product_option_value->id                                                     = 1;


        $opt                                                                            = array('resource' => 'combinations');
        $opt['postXml']                                                                 = $xml->asXML();
        sleep(1);
        $xml                                                                            = $webService->add($opt);
        $combination                                                                    = $xml->combination;
    } catch (PrestaShopWebserviceException $e){
        return;
    }
    //insert stock
    return $combination;
}
function make_product_options($data){

    $webService =$GLOBALS['webserv'];
    $config = PS_SHOP_PATH;

    try{
        $xml                                              = $webService->get(array('url' => $config.'api/product_option_values?schema=blank'));

        $product_option_value                             = $xml->children()->children();

        //$product_option_value->id                       = 1;
        $product_option_value->id_attribute_group         = $data["id_attribute_group"];

        $product_option_value->name->language[0][0]       = $data["name"];
        $product_option_value->name->language[0][0]['id'] = 1;


        $opt                                              = array('resource' => 'product_option_values');
        $opt['postXml']                                   = $xml->asXML();
        sleep(1);
        $xml                                              = $webService->add($opt);
        $product_option_value                             = $xml->product_option_value;
    } catch (PrestaShopWebserviceException $e){
        return 0;
    }
    //insert stock
    return (int) $product_option_value->id;

}

function make_father_product($data){
    $webService =$GLOBALS['webserv'];
    $config = PS_SHOP_PATH;

    try{
        $xml                                                                  = $webService->get(array('url' => $config.'api/products?schema=blank'));
        $product                                                              = $xml->children()->children();

        $product->price                                                       = $data["price"]; //Prix TTC
        $product->wholesale_price                                             =$data["price"]; //Prix d'achat
        $product->active                                                      = '1';
        $product->on_sale                                                     = 1; //on ne veux pas de bandeau promo
        $product->show_price                                                  = 1;
        $product->available_for_order                                         = 1;

        $product->name->language[0][0]                                        = $data["name"];
        $product->name->language[0][0]['id']                                  = 1;

        $product->description->language[0][0]                                 = $data["description"];
        $product->description->language[0][0]['id']                           = 1;

        $product->description_short->language[0][0]                           = $data["description"];
        $product->description_short->language[0][0]['id']                     = 1;
        $product->reference                                                   = $data["code"];

        $product->associations->categories->addChild('category')->addChild('id', $data["category_id"]);
        $product->id_category_default                                         = $data["category_id"];

        //$product->associations->stock_availables->stock_available->quantity = 1222;

        $opt                                                                  = array('resource' => 'products');
        $opt['postXml']                                                       = $xml->asXML();
        sleep(1);
        $xml                                                                  = $webService->add($opt);

        $product                                                              = $xml->product;
    } catch (PrestaShopWebserviceException $e){
        return;
    }
    return (int) $product->id;
}
